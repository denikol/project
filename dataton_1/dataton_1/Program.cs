﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dataton_1
{
    public class Rootobject
    {
        public string CardId { get; set; }
        public string Id { get; set; }
        public float Amount { get; set; }
        public string BankId { get; set; }
        public string Currency { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public Extrainfo ExtraInfo { get; set; }
        public string UserId { get; set; }
    }

    public class Extrainfo
    {
        public Categoryvalue[] CategoryValues { get; set; }
    }

    public class Categoryvalue
    {
        public float Percentage { get; set; }
        public string Type { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter User ID -> ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            string hashUserId = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            List<Rootobject> jsonResult;

            using(var reader = new StreamReader("PingFin.json"))
            {
                string json = reader.ReadToEnd();
                jsonResult = JsonConvert.DeserializeObject<List<Rootobject>>(json);
            }

            IEnumerable<Rootobject> mas;
            mas = jsonResult.Where(r => r.UserId == hashUserId);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (var m in mas)
            {
                if(m.ExtraInfo.CategoryValues == null)
                    continue;
                if (string.IsNullOrEmpty(m.Description)) 
                {
                    m.ExtraInfo.CategoryValues.FirstOrDefault().Type = "noname";
                }
                else if (m.Description.ToLower().Contains("shop") || m.Description.ToLower().Contains("market"))
                {
                    m.ExtraInfo.CategoryValues.FirstOrDefault().Type = "Food";
                }
                else if (m.Description.ToLower().Contains("uber") || m.Description.ToLower().Contains("taxi"))
                {
                    m.ExtraInfo.CategoryValues.FirstOrDefault().Type = "Taxi";
                }
                else if (m.Description.ToLower().Contains("apteca"))
                {
                    m.ExtraInfo.CategoryValues.FirstOrDefault().Type = "Health";
                }
                else if (m.Description.ToLower().Contains("bank") || m.Description.ToLower().Contains("mtb") || m.Description.ToLower().Contains("m6739113") || m.Description.ToLower().Contains("karta") || m.Description.ToLower().Contains("vklad") || m.Description.ToLower().Contains("perevod") || m.Description.ToLower().Contains("с вашего вклада") || m.Description.ToLower().Contains("sms opoveshenie") || m.Description.Contains("erip"))
                {
                    m.ExtraInfo.CategoryValues.FirstOrDefault().Type = "Bank";
                }
                Console.WriteLine("{0, -20}{1,-25}{2,-22}{3} руб.", m.Type, m.ExtraInfo.CategoryValues?.FirstOrDefault().Type, m.Date, m.Amount);
                //Console.WriteLine(m.Type + "\t\t" + m.ExtraInfo.CategoryValues?.FirstOrDefault().Type + "\t\t" + m.Date + "\t\t" + m.Amount + "руб."); 
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("Categories:");
            HashSet<string> typesHashSet = new HashSet<string>(); // HashSet - контейнер, принемающий только разные значения (повторы отбрасывает)
            foreach (var m in mas)
            {
                typesHashSet.Add(m.ExtraInfo.CategoryValues?.FirstOrDefault().Type);
            }
            int count = 0;
            typesHashSet.Remove(null); // Удаление пустых значений (обычно это первый элемент)
            foreach (var m in typesHashSet)
            {
                 Console.WriteLine(count + ". " + m);
                 count++;
            }
            List<string> typesList = new List<string>(typesHashSet); // Запись HashSet в List чтобы пользоваться индексацией (HashSet нет индексации)
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
            //var typeForSearch = "Food";
            Console.Write("Make a choice -> ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            int choice = int.Parse(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.Gray;
            var typeForSearch = typesList[choice];
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
            
            var arr1 = new List<DateTime>();

            mas = mas.Where(m => m.ExtraInfo.CategoryValues?.FirstOrDefault().Type == typeForSearch);

            foreach(var m in mas)
            {
                Console.WriteLine(m.Amount + " руб. " + "\t" + m.Date + "\t");
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
            var massArray = mas.ToArray();
            var diffAllDays = new List<double>();
            var diffFirstDecade = new List<double>();
            var diffSecondDecade = new List<double>();
            var diffTrirdDecade = new List<double>();

            for (var i = 0; i < massArray.Length-1; i++)
            {
                diffAllDays.Add(massArray[i+1].Date.Ticks - massArray[i].Date.Ticks);
                if (IsWithin(massArray[i + 1].Date.Day,1,10))
                {
                    diffFirstDecade.Add(massArray[i+1].Date.Ticks - massArray[i].Date.Ticks);
                }
                if (IsWithin(massArray[i + 1].Date.Day,11,20))
                {
                    diffSecondDecade.Add(massArray[i+1].Date.Ticks - massArray[i].Date.Ticks);
                }
                if (IsWithin(massArray[i + 1].Date.Day,21,31))
                {
                    diffTrirdDecade.Add(massArray[i+1].Date.Ticks - massArray[i].Date.Ticks);
                }
            }

            var tiksSumm = diffAllDays.Sum();
            var daysCount = diffAllDays.Count();

            if(diffAllDays.Count() == 0)
                Console.WriteLine("This user don't payed the "+ typeForSearch);
            else
            {
                var periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));
                
                var lastCostDate = mas.LastOrDefault().Date;
                var now = DateTime.Now;

                Console.WriteLine("All periods: maybe repeat after last spending in "+ lastCostDate.AddTicks(periodInTiks).ToString());
                Console.WriteLine("All periods: maybe repeat from now in "+ now.AddTicks(periodInTiks).ToString());

                if (IsWithin(lastCostDate.Day,1,10))
                {
                    tiksSumm = diffFirstDecade.Sum();
                    daysCount = diffFirstDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for first decade: maybe repeat in "+ lastCostDate.AddTicks(periodInTiks).ToString());
                }
                if (IsWithin(lastCostDate.Day,11,20))
                {
                    tiksSumm = diffSecondDecade.Sum();
                    daysCount = diffSecondDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for second decade: maybe repeat in "+ lastCostDate.AddTicks(periodInTiks).ToString());
                }
                if (IsWithin(lastCostDate.Day,21,31))
                {
                    tiksSumm = diffTrirdDecade.Sum();
                    daysCount = diffTrirdDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for third decade: maybe repeat in "+ lastCostDate.AddTicks(periodInTiks).ToString());
                }

                // FROM NOW
                
                if (IsWithin(now.Day,1,10))
                {
                    tiksSumm = diffFirstDecade.Sum();
                    daysCount = diffFirstDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for first decade: maybe repeat from NOW in "+ now.AddTicks(periodInTiks).ToString());
                }
                if (IsWithin(now.Day,11,20))
                {
                    tiksSumm = diffSecondDecade.Sum();
                    daysCount = diffSecondDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for second decade: maybe repeat from NOW in "+ now.AddTicks(periodInTiks).ToString());
                }
                if (IsWithin(now.Day,21,31))
                {
                    tiksSumm = diffTrirdDecade.Sum();
                    daysCount = diffTrirdDecade.Count();
                    periodInTiks = Convert.ToInt64(Math.Round(tiksSumm/daysCount));

                    Console.WriteLine("Periods for third decade: maybe repeat from NOW in "+ now.AddTicks(periodInTiks).ToString());
                }
            }
            
            Console.ReadKey();
        }

        private static bool IsWithin(int value, int minimum, int maximum)
        {
            return value >= minimum && value <= maximum;
        }
    }
}